# frozen_string_literal: true

# SequelAccount
module SequelAccount
  class << self
    attr_writer :min_password_length, :jwt_secret, :jwt_algorithm
    attr_accessor :db

    def setup
      yield self
      require_relative '../models/base_model'
      require_relative '../models/account'
      require_relative '../models/account_activity'
      require_relative '../models/account_jwt_key'
      require_relative '../models/account_password_hash'
      require_relative '../models/account_previous_password_hash'
      require_relative '../models/account_session_key'
    end

    def min_password_length
      @min_password_length || 8
    end

    def jwt_secret
      @jwt_secret || 'm!y$ecretK3!y'
    end

    def jwt_algorithm
      @jwt_algorithm || 'HS512'
    end
  end
end
