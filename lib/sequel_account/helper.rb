# frozen_string_literal: true

module SequelAccount
  # Helper
  class Helper
    attr_accessor :account

    def initialize(user)
      @account = user.account if user
    end

    def create_session_key(passwd)
      return nil unless @account&.authorized?(passwd)

      ::SequelAccount::Models::AccountSessionKey.create(account_id: @account.id)
    end

    def create_jwt(passwd, payload = {})
      return nil unless @account&.authorized?(passwd)

      ajk = ::SequelAccount::Models::AccountJwtKey.where(account_id: @account.id)
      ajk&.delete
      ::SequelAccount::Models::AccountJwtKey.create(account_id: @account.id, payload: payload)
    end

    def authorize_jwt(token)
      ajk = ::SequelAccount::Models::AccountJwtKey.find(account_id: @account.id, jwt_key: token)
      ajk&.decode
    end

    def authorize_session_key(key)
      ask = ::SequelAccount::Models::AccountSessionKey.find(account_id: @account.id, session_id: key)
      ask.account
    end

    def logout
      ask = ::SequelAccount::Models::AccountSessionKey.find(account_id: @account.id, session_id: @session[@session_key])
      @session.clear if ask.delete
    end
  end
end
