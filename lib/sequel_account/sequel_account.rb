# frozen_string_literal: true

module Sequel
  module Plugins
    # SequelAccount
    module SequelAccount
      def self.configure(model, opts = {}) # rubocop:disable Lint/UnusedMethodArgument
        model.instance_eval do
        end
      end

      # ClassMethods
      module ClassMethods
      end

      # InstanceMethods
      module InstanceMethods
        attr_accessor :password_confirmation, :password

        def password_valid?(passwd)
          account.authorized?(passwd)
        end

        def account
          ::SequelAccount::Models::Account.find(uuid: account_uuid)
        end

        private

        def create_account
          acc = ::SequelAccount::Models::Account.create(email: email, password: password,
                                                        password_confirmation: password_confirmation)
          self.account_uuid = acc.uuid
        end
      end
    end
  end
end
