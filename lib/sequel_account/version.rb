# frozen_string_literal: true

module Sequel
  module Plugins
    module SequelAccount
      VERSION = '0.0.2'
    end
  end
end
