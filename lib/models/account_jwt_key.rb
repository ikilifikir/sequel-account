# frozen_string_literal: true

# require_relative 'base_model'
require 'jwt'

module  SequelAccount
  module Models
    # AccountJwtKey class
    class AccountJwtKey < ::Sequel::Model(:sequel_account_jwt_key)
      plugin :validation_helpers
      plugin :singular_table_names
      many_to_one :account

      attr_accessor :payload

      unrestrict_primary_key

      def validate
        super
        validates_presence :account_id
      end

      def before_create
        self.jwt_key = JWT.encode @payload, SequelAccount.jwt_secret, SequelAccount.jwt_algorithm
      end

      def decode
        JWT.decode jwt_key, SequelAccount.jwt_secret, true, { algorithm: SequelAccount.jwt_algorithm }
      rescue StandardError
        # TODO: add logger instance
        nil
      end

      def self.decode(token)
        JWT.decode token, SequelAccount.jwt_secret, true, { algorithm: SequelAccount.jwt_algorithm }
      rescue StandardError
        # TODO: add logger instance
        nil
      end
    end
  end
end
