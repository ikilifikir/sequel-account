# frozen_string_literal: true

# require_relative 'base_model'

module SequelAccount
  module Models
    # AccountActivity class
    class AccountActivity < ::Sequel::Model(:sequel_account_activity)
      plugin :validation_helpers
      plugin :singular_table_names

      def self.add(account_id:, session_id:, login_ip: nil, login_at: nil, # rubocop:disable Metrics/ParameterLists
                   activity_ip: nil, activity_at: nil)
        activity = find_or_create(account_id, session_id)
        activity.last_login_at = login_at if login_at
        activity.last_login_ip = login_ip if login_ip
        activity.last_activity_at = activity_at if activity_at
        activity.last_activity_ip = activity_ip if activity_ip
        activity.save
      end

      class << self
        private

        def find_or_create(account_id, session_id)
          aa = AccountActivity.find(account_id: account_id, session_id: session_id)
          unless aa
            aa = AccountActivity.new
            aa.account_id = account_id
            aa.session_id = session_id
          end
          aa
        end
      end
    end
  end
end
