# frozen_string_literal: true

# require_relative 'base_model'
require 'securerandom'

module  SequelAccount
  module Models
    # AccountSessionKey class
    class AccountSessionKey < ::Sequel::Model(:sequel_account_session_key)
      plugin :validation_helpers
      plugin :singular_table_names
      many_to_one :account

      unrestrict_primary_key

      def before_create
        self.session_id = SecureRandom.alphanumeric(128)
      end
    end
  end
end
