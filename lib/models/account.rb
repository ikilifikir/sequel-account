# frozen_string_literal: true

# require_relative 'base_model'
require_relative 'account_password_hash'
require 'securerandom'
require 'digest'

module  SequelAccount
  module Models
    # Account class
    class Account < ::Sequel::Model(:sequel_account)
      plugin :validation_helpers
      plugin :singular_table_names
      one_to_many :account_session_keys
      one_to_many :account_jwt_keys
      attr_accessor :password, :password_confirmation

      def validate
        super
        validates_min_length 6, :password
        errors.add(:password, 'password not equeal to confirmation') if password && password != password_confirmation
        validates_includes [5, 10, 15], :status if status
        validates_presence %i[email password password_confirmation] if new?
      end

      def before_create
        self.status = 5
        self.uuid = ::SecureRandom.uuid
      end

      def after_save
        AccountPasswordHash.update_password(id, password)
      end

      def authorized?(password)
        AccountPasswordHash.find(id: id)&.authorized?(password)
      end

      def before_destroy
        AccountPasswordHash.find(id: id).destroy
        AccountActivity.where(account_id: id).each(&:destroy)
      end
    end
  end
end
