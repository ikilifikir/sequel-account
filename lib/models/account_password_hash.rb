# frozen_string_literal: true

# require_relative 'base_model'
require_relative 'account_previous_password_hash'

module SequelAccount
  module Models
    # AccountPasswordHash class
    class AccountPasswordHash < ::Sequel::Model(:sequel_account_password_hash)
      plugin :validation_helpers
      plugin :singular_table_names

      attr_accessor :password

      def before_save
        AccountPreviousPasswordHash.add_hash(id, digest_password(password))
        self.password_hash = digest_password(password)
      end

      def authorized?(password)
        password_hash == digest_password(password)
      end

      def self.update_password(account_id, password)
        aph = AccountPasswordHash.find(id: account_id)
        if aph
          aph.update(password: password)
        else
          aph = AccountPasswordHash.new(password: password)
          aph.id = account_id
          aph.save
        end
      end

      def digest_password(password)
        ::Digest::SHA512.base64digest(password)
      end

      def before_destroy
        AccountPreviousPasswordHash.where(id: id).each(&:destroy)
      end
    end
  end
end
