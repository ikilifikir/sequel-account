# frozen_string_literal: true

# require_relative 'base_model'

module SequelAccount
  module Models
    # AccountPreviousPasswordHash class
    class AccountPreviousPasswordHash < ::Sequel::Model(:sequel_account_previous_password_hash)
      plugin :validation_helpers
      plugin :singular_table_names

      def self.add_hash(account_id, p_hash)
        if AccountPreviousPasswordHash.where(account_id: account_id, password_hash: p_hash).count.positive?
          raise 'Password found in previous passwords!'
        end

        if AccountPreviousPasswordHash.where(account_id: account_id).count >= 4 # TODO: make this a setting
          top_ids = AccountPreviousPasswordHash.where(account_id: account_id).order(Sequel.desc(:id)).select(:id).limit(4).map(&:id) # rubocop:disable Layout/LineLength
          AccountPreviousPasswordHash.where(account_id: account_id).exclude(id: top_ids).delete
        end
        AccountPreviousPasswordHash.create(account_id: account_id, password_hash: p_hash)
      end
    end
  end
end
