# frozen_string_literal: true

require 'sequel'

module SequelAccount
  # Models module
  module Models
    BaseModel = Class.new(::Sequel::Model)
    BaseModel.def_Model(self)
    DB = BaseModel.db = ::SequelAccount.db
    BaseModel.plugin :validation_helpers
    BaseModel.plugin :singular_table_names
    # # BaseModel
    # class BaseModel
    #   plugin :validation_helpers
    # end
  end
end
