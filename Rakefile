# frozen_string_literal: true

require 'sequel'
require 'sqlite3'

DB = Sequel.connect('sqlite://sequel_account.db')
DB_MIGRATIONS_PATH = File.expand_path('spec/migrate', __dir__)

namespace :db do
  Sequel.extension :migration

  desc 'Drop database'
  task :drop do
    puts 'Dropping database ...'
    Sequel::Migrator.run(DB, DB_MIGRATIONS_PATH, target: 0)
  end

  desc 'Migrate database'
  task :migrate, [:version] do |_t, args|
    if args[:version]
      puts "Migrating to version #{args[:version]} ..."
      Sequel::Migrator.run(DB, DB_MIGRATIONS_PATH, target: args[:version].to_i, allow_missing_migration_files: true)
    else
      puts 'Migrating to the latest schema ...'
      Sequel::Migrator.run(DB, DB_MIGRATIONS_PATH, allow_missing_migration_files: true)
    end
  end
end
