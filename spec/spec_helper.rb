# frozen_string_literal: true

require 'rubygems'
require 'bundler'
Bundler.setup

require 'sequel'
require 'sequel/extensions/migration'
require 'net/http'
require_relative '../lib/sequel_account'

RSpec.configure do |c|
  c.before :suite do
    Sequel::Model.db = Sequel.sqlite # in memory
    Sequel.extension :migration
    Sequel::Model.plugin :singular_table_names
    Sequel::Migrator.run(Sequel::Model.db, File.expand_path('migrate', __dir__), target: 0)
    Sequel::Migrator.run(Sequel::Model.db, File.expand_path('migrate', __dir__))

    SequelAccount.setup do |config|
      config.min_password_length = 8
      config.db = Sequel::Model.db
    end

    class User < Sequel::Model # rubocop:disable Lint/ConstantDefinitionInBlock
      plugin :sequel_account

      def before_create
        create_account
      end
    end
  end
end
