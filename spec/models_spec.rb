# frozen_string_literal: true

require_relative 'spec_helper'
describe 'using SequelAccount::Account' do # rubocop:disable Metrics/BlockLength
  it 'should have an account with uuid' do
    u = User.create(email: 'a@b.com', password: '12345678', password_confirmation: '12345678')
    expect(u.account_uuid).to be_a(String)
  end

  it 'should authenticate with valid password' do
    u = User.find(email: 'a@b.com')
    expect(u.password_valid?('12345678')).to be(true)
  end

  it 'should not authenticate with invalid password' do
    u = User.find(email: 'a@b.com')
    expect(u.password_valid?('123457')).to be(false)
  end

  it 'should create a session key with correct password' do
    u = User.find(email: 'a@b.com')
    helper = SequelAccount::Helper.new(u)
    ask = helper.create_session_key('12345678')
    expect(ask.session_id.class).to be(String)
    expect(ask.class).to be(SequelAccount::Models::AccountSessionKey)
  end

  it 'should authorize with valid session key' do
    u = User.find(email: 'a@b.com')
    helper = SequelAccount::Helper.new(u)
    ask = SequelAccount::Helper.new(u).create_session_key('12345678')
    expect(helper.authorize_session_key(ask.session_id)).to be == u.account
  end

  it 'should create a valid jwt key with correct password' do
    u = User.find(email: 'a@b.com')
    ajk = SequelAccount::Helper.new(u).create_jwt('12345678')
    expect(SequelAccount::Models::AccountJwtKey.decode(ajk.jwt_key)[0]).to be == {}
  end

  it 'should create a valid jwt key with given payload' do
    u = User.find(email: 'a@b.com')
    custom_payload = { 'email' => u.email, 'test' => '123' }
    ajk = SequelAccount::Helper.new(u).create_jwt('12345678', custom_payload)
    expect(SequelAccount::Models::AccountJwtKey.decode(ajk.jwt_key)[0]).to be == custom_payload
  end

  it 'should have a valid jwt key and the jwt key shoul have account link' do
    u = User.find(email: 'a@b.com')
    ajk = SequelAccount::Helper.new(u).create_jwt('12345678')
    expect(ajk.account.uuid).to be == u.account.uuid
  end

  it 'should authorize with valid jwt key' do
    u = User.find(email: 'a@b.com')
    helper = SequelAccount::Helper.new(u)
    ajk = SequelAccount::Helper.new(u).create_jwt('12345678')
    expect(helper.authorize_jwt(ajk.jwt_key)).to be == [{}, { 'alg' => SequelAccount.jwt_algorithm }]
  end
end
