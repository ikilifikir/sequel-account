# frozen_string_literal: true

Sequel.migration do # rubocop:disable Metrics/BlockLength
  up do # rubocop:disable Metrics/BlockLength
    create_table('sequel_account') do
      primary_key   :id
      String        :uuid
      String        :email
      Integer       :status
      index         :uuid, unique: true
      index         :email, unique: true
    end

    create_table('sequel_account_password_hash') do
      foreign_key   :id, :sequel_account, primary_key: true
      String        :password_hash, null: false
    end

    create_table('sequel_account_previous_password_hash') do
      primary_key   :id
      foreign_key   :account_id, :sequel_account
      String        :password_hash, null: false
    end

    create_table('sequel_account_session_key') do
      foreign_key :account_id, :sequel_account
      String      :session_id
      Time        :created_at, null: false, default: Sequel::CURRENT_TIMESTAMP
      Time        :last_use, null: false, default: Sequel::CURRENT_TIMESTAMP
      primary_key %i[account_id session_id]
    end

    create_table('sequel_account_jwt_key') do
      foreign_key :account_id, :sequel_account
      String      :jwt_key
      Time        :created_at, null: false, default: Sequel::CURRENT_TIMESTAMP
      primary_key %i[account_id jwt_key]
    end

    create_table('sequel_account_login_count') do
      foreign_key :id, :sequel_account, primary_key: true
      Integer     :failed_login, null: false, default: 0
      Integer     :successful_login, null: false, default: 0
    end

    create_table('sequel_account_activity') do
      foreign_key :account_id, :sequel_account
      String      :session_id
      DateTime    :last_activity_at
      DateTime    :last_login_at
      String      :last_activity_ip
      String      :last_login_ip, null: false
      primary_key %i[account_id session_id]
    end

    create_table('sequel_account_password_change_time') do
      foreign_key :id, :sequel_account, primary_key: true
      DateTime    :changed_at, null: false, default: Sequel::CURRENT_TIMESTAMP
    end

    create_table('sequel_account_token') do
      primary_key :id
      foreign_key :account_id, :sequel_account
      Text        :token, null: false
      DateTime    :requested_at, null: false, default: Sequel::CURRENT_TIMESTAMP
      DateTime    :valid_until,  null: false
    end
  end

  down do
    drop_table(:sequel_account_previous_password_hash)
    drop_table(:sequel_account_password_hash)
    drop_table(:sequel_account_session_key)
    drop_table(:sequel_account_jwt_key)
    drop_table(:sequel_account_login_count)
    drop_table(:sequel_account_activity)
    drop_table(:sequel_account_password_change_time)
    drop_table(:sequel_account_token)
    drop_table(:sequel_account)
  end
end
