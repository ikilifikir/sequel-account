# frozen_string_literal: true

Sequel.migration do
  up do
    create_table(:user) do
      primary_key :id
      String :email, null: false
      String :account_uuid, null: false
      index :email, unique: true
      index :account_uuid, unique: true
    end
  end

  down do
    drop_table(:user)
  end
end
