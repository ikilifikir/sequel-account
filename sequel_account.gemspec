# frozen_string_literal: true

require_relative 'lib/sequel_account/version'

Gem::Specification.new do |s|
  s.required_ruby_version = '>= 2.7'
  s.name = 'sequel_account'
  s.version = Sequel::Plugins::SequelAccount::VERSION
  # s.date = Time.now.strftime('%Y-%m-%d')
  s.metadata['rubygems_mfa_required'] = 'true'
  s.summary = <<~EOF_SUMMARY
    Plugin to add authentication methods to Sequel Model
    Select one of crypt, bcrypt, scrypt
  EOF_SUMMARY
  s.authors = ['Fatih GENÇ']
  s.files = Dir['lib/**/*.rb']
  s.homepage = 'https://gitlab.com/ikilifikir/sequel-account'
  s.license = 'MIT'
  s.description = 'Plugin to add authentication methods to Sequel Model'
  s.email = 'fatihgnc@gmail.com'

  s.add_dependency 'bcrypt', '~> 3.1'
  s.add_dependency 'jwt', '~> 2.5'
  s.add_dependency 'scrypt', '~> 3.0'
  s.add_dependency 'sequel', '~> 5.0', '>= 5.0.0'

  s.add_development_dependency 'rspec', '~> 3.0'
  s.add_development_dependency 'simplecov', '~> 0.21'
  s.add_development_dependency 'sqlite3', '~> 1.4'
end
